package cabot.valentin.lab4.ex1;

public class Circle2 {
	
	private double radius = 1.0;
	
	private String color = "red";
	
	public Circle2() {
	}
	
	public Circle2(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return Math.PI * radius * radius;
	}
	
	public String toString() {
		return "r:" + radius + ", color:" + color;
	}
	
	
	public static void main(String[] args) {
		Circle2 circ1 = new Circle2();
		System.out.println(circ1);
		
		Circle2 circ2 = new Circle2(3);
		System.out.println(circ2);
		
		System.out.println(circ1.getArea());
		System.out.println(circ2.getArea());

	}

}
