package cabot.valentin.lab4.ex6;

public class Shape {
	
	private String color;
	private boolean filled;
	
	public Shape() {
		this.color = "red";
		this.filled = true;
	}
	
	public Shape(String color, boolean filled) {
		this.color = color;
		this.filled = filled;
	}

	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public boolean isFilled(){
		return filled;
	}
	
	public void setfilled(boolean filled) {
		this.filled = filled;
	}
	
	public String toString() {
		if (filled) {
			return "A Shape with the color " +  color + " and filled";
		}else {
			return "A Shape with the color " +  color + " and not filled";
		}
		
	}
}
