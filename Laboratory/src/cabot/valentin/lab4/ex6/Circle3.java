package cabot.valentin.lab4.ex6;

public class Circle3 extends Shape{
	
	private double radius;
	
	public Circle3() {
		super();
		this.radius = 1.0;
	}
	
	public Circle3(double radius) {
		super();
		this.radius = radius;
	}
	
	public Circle3(double radius, String color, boolean filled) {
		super(color, filled);
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getArea() {
		return Math.PI * radius * radius;
	}
	
	public double getPerimeter() {
		return Math.PI * this.getRadius();
	}
	
	@Override
	public String toString() {
		return "A Circle with radius=" + this.getRadius() + ", which is a subclass of " + super.toString();
	}
}
