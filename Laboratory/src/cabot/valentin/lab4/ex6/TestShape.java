package cabot.valentin.lab4.ex6;

public class TestShape {
	
	public static void main(String[] args) {
		//test
		Circle3 c1 = new Circle3(3, "red", true);
		System.out.println(c1);
		
		Rectangle r = new Rectangle(4, 5, "blue", false);
		System.out.println(r);
		r.setColor("green");
		r.setLength(2);
		System.out.println(r);
		
		Square s = new Square(5, "purple", true);
		System.out.println(s);
		s.setSide(2);
		System.out.println(s);
		
		s.setLength(3);
		s.setWidth(9);
		System.out.println(s);
	}
}
