package cabot.valentin.lab4.ex4;

import cabot.valentin.lab4.ex2.*;

public class BetterBook {
	
	private String name;
	private Author2[] author;
	private double price;
	private int qtyInStock = 0;
	
	public BetterBook(String name, Author2[] author, double price) {
		this.name = name;
		this.author = author;
		this.price = price;
	}
	
	public BetterBook(String name, Author2[] author, double price, int qtyInStock) {
		this.name = name;
		this.author = author;
		this.price = price;
		this.qtyInStock = qtyInStock; 
	}
	
	public String getName() {
		return name;
	}
	
	public Author2[] getAuthor() {
		return author;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	public void printAuthors() {
		for(int i=0; i<author.length; i++) {
			System.out.println("author " + (i+1) + ":" + author[i]);
		}
	}
	
	public static void main(String[] args) {
		Author2 val = new Author2("Valentin", "valentin.bouchentouf@gmail.com", 'm');
		Author2 phip = new Author2("phiphi", "phiphi.voila@gmail.com", 'f');
		Author2 alex = new Author2("alexi", "alll.xii@yahoo.com", 'f');
		
		Author2[] liste = {val, phip, alex};
		
		BetterBook b1 = new BetterBook("the story of java", liste, 999.0);
		System.out.println(b1.getName());
		
		b1.printAuthors();
		System.out.println(b1.getPrice());
		System.out.println(b1.getQtyInStock());

	}

}
