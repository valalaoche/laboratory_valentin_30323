package cabot.valentin.lab4.ex3;

import cabot.valentin.lab4.ex2.*;

public class Book {
	
	private String name;
	private Author2 author;
	private double price;
	private int qtyInStock = 0;
	
	public Book(String name, Author2 author, double price) {
		this.name = name;
		this.author = author;
		this.price = price;
	}
	
	public Book(String name, Author2 author, double price, int qtyInStock) {
		this.name = name;
		this.author = author;
		this.price = price;
		this.qtyInStock = qtyInStock; 
	}
	
	public String getName() {
		return name;
	}
	
	public Author2 getAuthor() {
		return author;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	public static void main(String[] args) {
		Author2 val = new Author2("Valentin", "valentin.bouchentouf@gmail.com", 'm');
		Book b1 = new Book("the story of java", val, 999.0);
		System.out.println(b1.getName());
		System.out.println(b1.getAuthor());
		System.out.println(b1.getPrice());
		System.out.println(b1.getQtyInStock());
		
		b1.setQtyInStock(100);
		System.out.println(b1.getQtyInStock());
		
		Author2 phip = new Author2("phiphi", "phiphi.voila@gmail.com", 'f');
		Book b2 = new Book("the story of pyhon", phip, 9.9, 20);
		System.out.println(b2.getName());
		System.out.println(b2.getAuthor());
		System.out.println(b2.getPrice());
		System.out.println(b2.getQtyInStock());
		
		b2.setPrice(1000);
		System.out.println(b2.getPrice());

	}

}
