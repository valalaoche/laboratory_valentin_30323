package cabot.valentin.lab4.ex2;

public class Author2 {
	
	private String name;
	private String email;
	private char gender;
	
	public Author2 (String name, String email, char gender) {
		this.name = name;
		this.email = email;
		this.gender = gender;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public char getGender() {
		return gender;
	}
	
	public String toString() {
		return name + " (" + gender + ") at " + email;
	}
	
	public static void main(String[] args) {
		Author2 val = new Author2("Valentin", "valentin.bouchentouf@gmail.com", 'm');
		System.out.println(val);
		val.setEmail("valentin.2222@gmail.com");
		System.out.println(val);
	}
}
