package cabot.valentin.lab3.ex2;

public class Circle {

	private double radius = 1.0;
	
	private String color = "red";
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public Circle(double radius, String color) {
		this.radius = radius;
		this.color = color;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return Math.PI * radius * radius;
	}
	
	public String getColor() {
		return color;
	}
	
	public static void main(String[] args) {
		
		Circle circ1 = new Circle(1);
		System.out.println("circle 1 radius:" + circ1.getRadius());
		System.out.println("circle 2 Area:" + circ1.getArea());
		
		
		Circle circ2 = new Circle(3, "Blue");
		System.out.println("circle 2 radius:" + circ2.getRadius());
		System.out.println("circle 2 Area:" + circ2.getArea());
		System.out.println("circle 2 color:" + circ2.getColor());
	}
}


