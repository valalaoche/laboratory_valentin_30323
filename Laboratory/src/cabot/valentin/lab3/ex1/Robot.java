package cabot.valentin.lab3.ex1;

public class Robot {
	
	private int x;
	
	public Robot() {
		this.x = 1;
	}
	
	public void change(int k) {
		if (k >= 1) {
			this.x += k;
		}
	}
	
	public String toString() {
		return String.valueOf(this.x);
	}


	public static void main(String[] args) {
		Robot rob = new Robot();
		rob.change(3);
		System.out.println(rob.toString());

	}

}

