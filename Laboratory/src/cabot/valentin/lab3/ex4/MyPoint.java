package cabot.valentin.lab3.ex4;

import java.lang.Math;

public class MyPoint {

	public int x;
	public int y;

	public MyPoint() {
		x = 0;
		y = 0;
	}
	
	public MyPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		return "(" + x + "," + y + ")";
	}
	
	public double distance(int x, int y) {
		return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
	}
	
	public double distance(MyPoint another) {
		return Math.sqrt(Math.pow(this.x - another.getX(), 2) + Math.pow(this.y - another.getY(), 2));
	}
	
	public static void main(String[] args) {
		MyPoint p1= new MyPoint();
		System.out.println(p1.toString());
		
		MyPoint p2= new MyPoint(2, 3);
		System.out.println(p2.toString());
		
		p1.setX(1);
		p1.setY(2);
		System.out.println(p1.toString());
		
		p2.setXY(-1, -2);
		System.out.println(p2.toString());
		
		System.out.println(p1.distance(1, 1));
		
		System.out.println(p1.distance(p2));
		
		
	}
}
