package cabot.valentin.lab5.ex3;

import java.util.concurrent.ThreadLocalRandom;

public class LightSensor extends Sensor{
	
	public LightSensor() {
		super();
	}
	
	@Override
	public int readValue() {
		return ThreadLocalRandom.current().nextInt(0, 101);
	}
}
