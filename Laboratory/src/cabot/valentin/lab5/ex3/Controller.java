package cabot.valentin.lab5.ex3;

public class Controller {
	LightSensor ls;
	TemperatureSensor ts;
	
	public Controller() {
		ls = new LightSensor();
		ts = new TemperatureSensor();
	}
	
	public void control() throws InterruptedException {
		for(int i=0; i<20; i++) {
			System.out.println(i+1 + ") light:" + ls.readValue() + ", Temperature:" + ts.readValue());
			Thread.sleep(1000);
		}
	}
}
