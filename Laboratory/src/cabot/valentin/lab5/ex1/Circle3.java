package cabot.valentin.lab5.ex1;

public class Circle3 extends Shape{
	
	protected double radius;
	
	public Circle3() {
		super();
		this.radius = 1.0;
	}
	
	public Circle3(double radius) {
		super();
		this.radius = radius;
	}
	
	public Circle3(double radius, String color, boolean filled) {
		super(color, filled);
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	@Override
	public double getArea() {
		return Math.PI * radius * radius;
	}
	
	@Override
	public double getPerimeter() {
		return Math.PI * this.getRadius();
	}
	
	@Override
	public String toString() {
		return "A Circle with radius=" + this.getRadius() + ", which is a subclass of " + super.toString();
	}
}
