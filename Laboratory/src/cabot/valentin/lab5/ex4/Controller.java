package cabot.valentin.lab5.ex4;

public class Controller {
	LightSensor ls = new LightSensor();
	TemperatureSensor ts = new TemperatureSensor();
	
	private static Controller cont;
	
	private Controller() {
	}
	
	public static Controller getInstance() {
		if(cont == null) {
			cont = new Controller();
		}
		return cont;
	}
	
	public void control() throws InterruptedException {
		for(int i=0; i<20; i++) {
			System.out.println(i+1 + ") light:" + ls.readValue() + ", Temperature:" + ts.readValue());
			Thread.sleep(1000);
		}
	}
}
