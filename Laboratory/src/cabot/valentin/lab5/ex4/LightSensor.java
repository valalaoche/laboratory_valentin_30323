package cabot.valentin.lab5.ex4;

import java.util.concurrent.ThreadLocalRandom;

public class LightSensor extends Sensor{
	@Override
	public int readValue() {
		return ThreadLocalRandom.current().nextInt(0, 101);
	}
}
