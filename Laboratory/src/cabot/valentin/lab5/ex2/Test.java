package cabot.valentin.lab5.ex2;

public class Test {
	public static void main(String[] args) {
		ProxyImage i1 = new ProxyImage("file_name_1234", "rotatedImage");
		i1.display();
		
		ProxyImage i2 = new ProxyImage("file_name_5678", "realImage");
		i2.display();
	}
}
