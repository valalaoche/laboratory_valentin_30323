package cabot.valentin.lab5.ex2;

public class ProxyImage implements Image{
	private RealImage realImage;
	private RotatedImage rotatedImage;
	private String fileName;
	private String imageType;
	 
	public ProxyImage(String fileName, String imageType){
		this.fileName = fileName;
		this.imageType = imageType;
	}
	 
	@Override
	public void display() {
		switch(imageType) {
			case "realImage":
				if(realImage == null){
					realImage = new RealImage(fileName);
			    }
			    realImage.display();
			    break;
			case "rotatedImage":
				if(rotatedImage == null){
					rotatedImage = new RotatedImage(fileName);
			    }
				rotatedImage.display();
			    break;
		
		}
		
	}
}
