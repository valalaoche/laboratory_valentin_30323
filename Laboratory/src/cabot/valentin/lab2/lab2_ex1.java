package cabot.valentin.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class lab2_ex1 {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter Integer:");
		int i = Integer.parseInt(br.readLine());
		System.out.print("Enter an other Integer:");
		int j = Integer.parseInt(br.readLine());
		if (i >= j) {
			System.out.println(i);
		} else {
			System.out.println(j);
		}
	}
}
