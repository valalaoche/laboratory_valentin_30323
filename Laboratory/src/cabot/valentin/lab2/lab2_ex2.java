package cabot.valentin.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class lab2_ex2 {

	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter Integer:");
		int i = Integer.parseInt(br.readLine());
		
		if (i == 1) {
			System.out.println("One");
		} else if (i == 2) {
			System.out.println("TWO");
		} else if (i == 3) {
			System.out.println("TREE");
		} else if (i == 4) {
			System.out.println("FOUR");
		} else if (i == 5) {
			System.out.println("FIVE");
		} else if (i == 6) {
			System.out.println("SIX");
		} else if (i == 7) {
			System.out.println("SEVEN");
		} else if (i == 8) {
			System.out.println("EIGHT");
		} else if (i == 9) {
			System.out.println("NINE");
		} else {
			System.out.println("OTHER");
		}

	}

}
