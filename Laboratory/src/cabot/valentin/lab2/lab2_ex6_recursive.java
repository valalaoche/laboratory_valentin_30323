package cabot.valentin.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class lab2_ex6_recursive {

	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter N:");
		int n = Integer.parseInt(br.readLine());
		
		System.out.println(n + "N! = " + factorial(n));
	}
	
	public static int factorial(int n) {
		if (n <= 1) {
			return 1;
		} else {
			return n * factorial(n - 1);
		}
	}
}
