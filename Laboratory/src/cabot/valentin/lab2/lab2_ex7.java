package cabot.valentin.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ThreadLocalRandom;

public class lab2_ex7 {

	public static void main(String[] args) throws IOException{
		int nb_guess = 3;
		int secret_nb = ThreadLocalRandom.current().nextInt(0, 10);
		int guess;
		boolean is_win = false;
		
		System.out.println("->find a number between 0 and 9, you have 3 chances !");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (nb_guess > 0 && is_win == false) {
			System.out.print("Guess:");
			guess = Integer.parseInt(br.readLine());
			if (guess > secret_nb) {
				nb_guess --;
				System.out.println("->Wrong answer, your number it too high");
			} else if (guess < secret_nb) {
				nb_guess --;
				System.out.println("->Wrong answer, your number is too low");
			} else {
				is_win = true;
			}
		}
		if (is_win == true) {
			System.out.println("you win !!");
		} else {
			System.out.println("you loose ... the number was:" + secret_nb);
		}

	}

}
