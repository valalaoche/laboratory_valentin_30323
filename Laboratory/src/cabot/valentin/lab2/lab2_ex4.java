package cabot.valentin.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class lab2_ex4 {

	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter N:");
		int n = Integer.parseInt(br.readLine());
		int[] vector = new int[n];
				
		for(int i=0; i<n; i++) {
			System.out.print("Enter an int:");
			vector[i] = Integer.parseInt(br.readLine());
		}
		int max = vector[0];
		for(int i=1; i<n; i++) {
			if (max < vector[i]) {
				max = vector[i];
			}
		}
		System.out.println("the max value of the vector is:" + max);
	}
}
