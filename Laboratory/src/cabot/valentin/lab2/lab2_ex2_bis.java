package cabot.valentin.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class lab2_ex2_bis {

	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter Integer:");
		int i = Integer.parseInt(br.readLine());
		String number;
		switch(i) {
			case 1: number = "ONE";
					break;
			case 2: number = "TWO";
					break;
			case 3: number = "THREE";
					break;
			case 4: number = "FOUR";
					break;
			case 5: number = "FIVE";
					break;
			case 6: number = "SIX";
					break;
			case 7: number = "SEVEN";
					break;
			case 8: number = "EIGHT";
					break;
			case 9: number = "NINE";
					break;
			default: number = "OTHER";
		}
		System.out.println(number);
	}

}
