package cabot.valentin.lab2;

import java.util.concurrent.ThreadLocalRandom;

public class lab2_ex5 {

	public static void main(String[] args) {
		int[] list = new int[10];
		for(int i=0; i<10; i++) {
			list[i] = ThreadLocalRandom.current().nextInt(-100, 101);
		}
		// display the list
		System.out.println("unsorted list:");
		for(int i=0; i<10; i++) {System.out.println(list[i] + " ");}
		// bubble sorting algorithm
		boolean is_sort = false;
		while(is_sort == false) {
			is_sort = true;
			for(int i=0; i<9; i++) {
				if(list[i] > list[i+1]) {
					//switch
					int temp = list[i+1];
					list[i+1] = list[i];
					list[i] = temp;
					is_sort = false;
				}
			}
		}
		// display the list
		System.out.println("sorted list:");
		for(int i=0; i<10; i++) {System.out.println(list[i] + " ");}
	}
}
