package cabot.valentin.lab2.ex5;

public class Flower{
    
    private static int nbFlower;
    
    public Flower(){ 
    	System.out.println("Flower has been created!");      
    	nbFlower ++;
    }
    
    public static int getNbFlower() {
    	return nbFlower;
    }
    public static void main(String[] args) {
    	Flower[] garden = new Flower[5];
    	for(int i =0;i<5;i++){
    	Flower f = new Flower();
    	garden[i] = f;
    	}
    	System.out.println("number of flower created:" + Flower.getNbFlower());
    }
}
