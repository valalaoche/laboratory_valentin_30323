package cabot.valentin.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class lab2_ex3 {
	
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter A:");
		int a = Integer.parseInt(br.readLine());
		System.out.print("Enter B:");
		int b = Integer.parseInt(br.readLine());
		
		int[] list = new int[b];
		list[0] = 2;
		int index = 1;
		
		for (int i=2; i<b; i++) {
			boolean is_prime = true;
			for (int j_index = 0; j_index<index; j_index++) {							
				if (i % list[j_index] == 0) {
					is_prime = false;					
					break;
				}
			}
			if (is_prime){
					list[index] = i;
					index ++;					
			}
		}
		int count = 0;  // display only the prime numbers between a and b
		for (int i = 0; i<index; i++) {
			if (a <= list[i] && b >= list[i]) {
				System.out.println(list[i]);
				count ++;
			}
		}
		System.out.println("there is " + count + " prime number between " + a + " and " + b);
	}
}
