package cabot.valentin.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class lab2_ex6 {

	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter N:");
		int n = Integer.parseInt(br.readLine());
		int result = 1;
		for(int i=2; i<n+1; i++) {
			result = result * i;
		}
		System.out.println(n + "! = " + result);
	}

}